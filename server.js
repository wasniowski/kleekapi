// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 80;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    // do logging
    console.log('Przetwarzam zapytanie');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

// more routes for our API will happen here
// on routes that end in /tickets
// ----------------------------------------------------
router.route('/tickets')

// create a ticket (accessed at POST http://localhost:8080/api/tickets)
    .post(function(req, res) {

        var ticket = new Ticket();      // create a new instance of the Ticket model
        ticket.name = req.body.name;
        ticket.price = req.body.price;
        ticket.quantity = req.body.quantity;
        ticket.quantityMax = req.body.quantityMax;
        ticket.capacity = req.body.capacity;
        ticket.sold = req.body.sold;
        ticket.checkedin = req.body.checkedin;
        ticket.deny = req.body.deny;

        // save the ticket and check for errors
        ticket.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Ticket created!' });
        });
    })

    .get(function(req, res) {
        Ticket.find(function(err, tickets) {
            if (err)
                res.send(err);

            res.json(tickets);
        });
    });

router.route('/tickets/:ticket_id')

// get the ticket with that id (accessed at GET http://localhost:8080/api/tickets/:ticket_id)
    .get(function(req, res) {
        Ticket.findById(req.params.ticket_id, function(err, ticket) {
            if (err)
                res.send(err);
            res.json(ticket);
        });
    })

    // update the ticket with this id (accessed at PUT http://localhost:8080/api/tickets/:ticket_id)
    .put(function(req, res) {

        // use our ticket model to find the ticket we want
        Ticket.findById(req.params.ticket_id, function(err, ticket) {

            if (err)
                res.send(err);

            ticket.name = req.body.name;
            ticket.price = req.body.price;
            ticket.quantity = req.body.quantity;
            ticket.quantityMax = req.body.quantityMax;
            ticket.capacity = req.body.capacity;
            ticket.sold = req.body.sold;
            ticket.checkedin = req.body.checkedin;
            ticket.deny = req.body.deny;

            // save the ticket
            ticket.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Ticket updated!' });
            });

        });
    })

    .delete(function(req, res) {
        Ticket.remove({
            _id: req.params.ticket_id
        }, function(err, ticket) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });

router.route('/users')

    .post(function(req, res) {

        var user = new User();

        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.oneliner = req.body.oneliner;
        user.promotor.id = req.body.promotorid;
        user.promotor.name = req.body.promotorname;

        // save the ticketUser and check for errors
        user.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'User created!' });
        });
    })

    .get(function(req, res) {
        User.find(function(err, user) {
            if (err)
                res.send(err);

            res.json(user);
        });
    });

router.route('/users/:user_id')

    .get(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            if (err)
                res.send(err);
            res.json(user);
        });
    })

    // update the ticket with this id (accessed at PUT http://localhost:8080/api/tickets/:ticket_id)
    .put(function(req, res) {

        // use our ticket model to find the ticket we want
        User.findById(req.params.user_id, function(err, user) {

            console.log(req.body);

            if (err)
                res.send(err);

            // save the ticket
            user.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'User updated!' });
            });

        });
    });

router.route('/users/:user_id/checkin')
    .get(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            user.status.type = 'checkedin';
            user.status.text = 'checkedin';
            user.status.time = new Date();

            user.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'User checkedin' });
            });
        })
    });

router.route('/promotors')

    .post(function(req, res) {

        var promotor = new Promotor();

        promotor.name = req.body.name;

        // save the promotor and check for errors
        promotor.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Promotor created!' });
        });
    })

    .get(function (req, res) {
        Promotor.find(function(err, promotors) {
            if (err)
                res.send(err);

            res.json(promotors);
        });
    });



router.route('/promotors/:promotor_id')
    .get(function(req, res) {
        Promotor.findById(req.params.promotor_id, function(err, promotor) {
            if (err)
                res.send(err);
            res.json(promotor);
        });
    });

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);

// MONGO
var mongoose   = require('mongoose');
mongoose.connect('mongodb://kamil:kamil@ds021984.mlab.com:21984/kleekdoor',{authMechanism: 'ScramSHA1'}); // connect to our database

mongoose.connection.on('error', function (err) {
    console.log(err)
});
mongoose.connection.on('connected', function (err) {
    console.log('Połączono')
});

var Ticket       = require('./model/ticket');
var User         = require('./model/user');
var Promotor     = require('./model/promotor');

