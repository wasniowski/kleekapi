var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PromotorSchema   = new Schema({
    name: String,
});

module.exports = mongoose.model('Promotor', PromotorSchema);