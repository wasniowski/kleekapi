var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var TicketSchema   = new Schema({
    name: String,
    price: Number,
    quantity: Number,
    quantityMax: Number,
    capacity: Number,
    sold: Number,
    checkedin: Number,
    deny: Number
});

module.exports = mongoose.model('Ticket', TicketSchema);