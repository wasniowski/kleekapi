var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
    firstName: String,
    lastName: String,
    oneliner: String,
    promotor: {
        id: String,
        name: String
    },
    order: {
        tickets: {
            sumQuantity: Number,
            sumCapacity: Number,
            items: [
                {
                    name: String,
                    price: Number,
                    quantity: Number,
                    capacity: Number
                }
            ]
        },
        entries: {
            sumQuantity: Number,
            sumCapacity: Number,
            items: [
                {
                    type: String,
                    name: String,
                    entry: {
                        female: {
                            price: Number,
                            quantity: Number
                        },
                        male: {
                            price: Number,
                            quantity: Number
                        }
                    }
                }
            ]
        }
    },
    status: {
        type: {type: String, default: 'waiting'},
        text: {type: String, default: 'default'},
        reason: String,
        time: Date
    }

});

module.exports = mongoose.model('User', UserSchema);
